# Skinny Widgets Menu


menu element

```
npm i sk-menu sk-menu-jquery --save
```

then add the following to your html

```html
<sk-config
    theme="jquery"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-jquery"
    lang="ru"
    id="skConfig"
></sk-config>
<sk-menu id="skMenu">
    <sk-menu-item>foo</sk-menu-item>
    <sk-menu-item>bar</sk-menu-item>
</sk-menu>
<script type="module">
    import { SkConfig } from '/node_modules/sk-config/src/sk-config.js';
    import { SkMenu } from '/node_modules/sk-menu/src/sk-menu.js';

    customElements.define('sk-config', SkConfig);
    customElements.define('sk-menu', SkMenu);
</script>
```

#### attributes

#### events

**itemclick** - triggered when menu item is clicked, detail: item - dom obj of item is clicked, origEvent - mouse click event 


#### template

id: SkMenuTpl