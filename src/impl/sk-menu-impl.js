
import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';
import { ItemclickEvent } from "../event/itemclick-event.js";


export class SkMenuImpl extends SkComponentImpl {

    get suffix() {
        return 'menu';
    }

    beforeRendered() {
        super.beforeRendered();
        this.saveState();
    }

    get body() {
        if (! this._body) {
            this._body = this.comp.el.querySelector('ul');
        }
        return this._body;
    }

    set body(el) {
        this._body = el;
    }

    get subEls() {
        return [ 'body' ];
    }

    dumpState() {
        return {
            'contents': this.comp.contentsState
        }
    }

    restoreState(state) {
        this.body.innerHTML = '';
        this.body.insertAdjacentHTML('beforeend', state.contentsState);
        this.renderItems();
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
    }

    renderItems() {
        let items = this.comp.el.querySelectorAll(this.comp.itemTn);
        for (let item of items) {
            let tpl = this.comp.renderer.prepareTemplate(this.comp.itemTpl);
            let html = this.comp.renderer.renderMustacheVars(tpl, { contents: item.innerHTML });
            item.innerHTML = html;
            item.addEventListener('click', function(event) {
                this.comp.dispatchEvent(new ItemclickEvent({ detail: { origEvent: event, item: item } }));
            }.bind(this));
        }
    }
}