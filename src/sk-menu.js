
import { SkElement } from '../../sk-core/src/sk-element.js';


export class SkMenu extends SkElement {

    get cnSuffix() {
        return 'menu';
    }

    get itemTn() {
        return this.getAttribute('item-tn') || 'sk-menu-item';
    }

    set itemTn(itemTn) {
        return this.setAttribute('item-tn', itemTn);
    }

    get itemTplPath() {
        if (! this._itemTplPath) {
            this._itemTplPath = this.confValOrDefault('tpl-path', `/node_modules/sk-menu-${this.theme}/src/`) + `${this.theme}-sk-menu-item.tpl.html`;
        }
        return this._itemTplPath;
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'disabled' && oldValue !== newValue) {
            if (newValue) {
                this.impl.disable();
            } else {
                this.impl.enable();
            }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
    }

    hide() {
        this.style.display = 'none';
    }

    show(event) {
        this.style.position = 'fixed';
        this.style.display = 'block';
        this.style.top = `${event.clientY}px`;
        this.style.left = `${event.clientX}px`;
        if (! this.mouseLeaveHandler) {
            this.mouseLeaveHandler = function() {
                this.hide();
            }.bind(this);
            this.onmouseleave = this.mouseLeaveHandler;
        }

    }

    static get observedAttributes() {
        return [ 'disabled' ];
    }

    get preLoadedTpls() {
        return [ 'itemTpl' ];
    }

}
