

export const ITEMCLICK_EVT = 'itemclick';

export class ItemclickEvent extends CustomEvent {
    constructor(options) {
        super(ITEMCLICK_EVT, options);
    }
}
